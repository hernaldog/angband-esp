# Angband-esp
Proyecto traducción **Angband 4.2.3 de Windows** al idioma español.

Página directa de binario y fuentes del Angband 4.2.3 original: https://rephial.org/

![Angband cover español](https://darknromhacking.com/Angband/angband-esp-1.png)

## Autor
Hernaldo González (hernaldog@gmail.com)

## Licencia
Software libre, la misma de Angband https://angband.readthedocs.io/en/latest/hacking/compiling.html

## Status del proyecto
0%: 20-09-2021 Partiendo recién en ver como compilar en Windows y herramientas.

## Bajar e instalar MinGW
Bajar **MinGW** https://osdn.net/projects/mingw/downloads/68260/mingw-get-setup.exe/.

Como instalar:
- Primera pantalla **Install**
- Segunda pantalla, selecciona directorio por defecto **C:\MinGW** y mantener los check y opciones de abajo. Presionar **Continue**. 
- Se cargará una barra verde, en unos 2 minutos llegará al 100%, presionar **Continue**
- Se abrirá una ventana llamada **MinGW Installation Manager**. En sección **Basic Setup**, a la derecha selecciona con un clic **Mark for installation** sobre: **mingw32-base-bin** y **mingw32-gcc-g++-bin**.
- Luego ir a opción superior **Installation**, **Apply changes**. Saldrá una ventana que dira **Okay for proceed?** Presionar **Apply** y comenzará la instalación.
- Luego de unos minutos dirá **All changes were applied successfully**, presionar **close**
- Para comprobar ve al disco duro y ve la carpeta **C:\MinGW\bin** y adentro deben estar los archivos **mingw32-make.exe** y **gcc.exe**, que son los que usaremos.

## Compilación
Ya con MingGW instalado, agregar la ruta **C:\MinGW\bin** en la variable de ambiente de Sistema o de Usuario **PATH**. También crea una nueva variable de Ambiente de Sistema con nombre **MINGW**, valor **yes**.

Para compilar ir por CMD y ve a la carpeta SRC. Ejemplo: cd C:\traduc\Angband-4.2.3\src

Ejecutar comando:

`mingw32-make -f Makefile.win`

Compilará, generará archivos .o y al final de unos minutos dirá algo así:

```
gcc -DWINDOWS -static -Iwin/include -Lwin/lib -Wall -Wextra -Wno-unused-parameter -Wno-missing-field-initializers -std=c99 -DUSE_PRIVATE_PATHS -DSOUND -O2 -I. -c -o buildid.o buildid.c
gcc -DWINDOWS -static -Iwin/include -Lwin/lib -Wall -Wextra -Wno-unused-parameter -Wno-missing-field-initializers -std=c99 -DUSE_PRIVATE_PATHS -DSOUND -O2 -I. -o angband.exe cave.o cave-map.o cave-square.o cave-view.o cmd-cave.o cmd-core.o cmd-misc.o cmd-obj.o cmd-pickup.o cmd-spoil.o cmd-wizard.o datafile.o debug.o effects.o effects-info.o game-event.o game-input.o game-world.o generate.o gen-cave.o gen-chunk.o gen-monster.o gen-room.o gen-util.o grafmode.o guid.o init.o load.o message.o mon-attack.o mon-blows.o mon-desc.o mon-group.o mon-init.o mon-list.o mon-lore.o mon-make.o mon-move.o mon-msg.o mon-predicate.o mon-spell.o mon-summon.o mon-timed.o mon-util.o obj-chest.o obj-curse.o obj-desc.o obj-gear.o obj-ignore.o obj-info.o obj-init.o obj-knowledge.o obj-list.o obj-make.o obj-pile.o obj-power.o obj-properties.o obj-randart.o obj-slays.o obj-tval.o obj-util.o option.o parser.o randname.o player-attack.o player-birth.o player-calcs.o player-class.o player-history.o player-path.o player-properties.o player-quest.o player-race.o player-spell.o player-timed.o player-util.o player.o project.o project-feat.o project-mon.o project-obj.o project-player.o score.o save.o savefile.o save-charoutput.o sound-core.o source.o store.o target.o trap.o ui-birth.o ui-command.o ui-context.o ui-curse.o ui-death.o ui-display.o ui-effect.o ui-entry.o ui-entry-combiner.o ui-entry-renderers.o ui-equip-cmp.o ui-event.o ui-game.o ui-help.o ui-history.o ui-init.o ui-input.o ui-keymap.o ui-knowledge.o ui-map.o ui-menu.o ui-mon-list.o ui-mon-lore.o ui-obj-list.o ui-object.o ui-options.o ui-output.o ui-player.o ui-prefs.o ui-score.o ui-signals.o ui-spell.o ui-spoil.o ui-store.o ui-target.o ui-term.o ui-visuals.o ui-wizard.o wiz-debug.o wiz-spoil.o wiz-stats.o  buildid.o z-bitflag.o z-color.o z-dice.o z-expression.o z-file.o z-form.o z-quark.o z-queue.o z-rand.o z-set.o z-textblock.o z-type.o z-util.o z-virt.o win/angband.res  main-win.o win/readdib.o win/readpng.o win/scrnshot.o win/win-layout.o -mwindows -lwinmm -lzlib -llibpng -lmsimg32
copy angband.exe ..
        1 archivo(s) copiado(s).
copy win"\"dll"\"libpng12.dll ..
El sistema no puede encontrar el archivo especificado.
Makefile.win:116: recipe for target '../angband.exe' failed
mingw32-make: *** [../angband.exe] Error 1
```
El archivo final binario compilado quedará en esa misma carpeta **angband.exe**.

## Probando exe
Descarga el binario para Windows Angband-4.2.3-win.zip de https://rephial.org/ donde dice "Download". De todas formas, en caso que lo muevan o reemplacen lo respaldé aca: https://darknromhacking.com/archivos/Angband-4.2.3-win.zip

Descomprimir en c:\Angband-4.2.3

Para probarlo, copia ese angband.exe que generaste, a la carpeta donde dejaste el binario anterior, reemplaza el exe por el generado por ti. Así toma las librerías .dll correctas.

## compilayejecuta.bat
Puedes editar y ejecutar el archivo **compilayejecuta.bat** que está en la raiz del código fuente. Lo puedes modificar a gusto para que haga las tareas de compilar, copiar nuevo angband.exe creado a la ruta correcta y ejecutar el juego.

```
cd src

:: Compilamos
mingw32-make -f Makefile.win

:: copiamos ejecutable
xcopy angband.exe D:\traduc\Angband-4.2.3\angband.exe /Y

:: Copia de archivos txt como portada, etc
cd ..
cd lib
cd screens
xcopy *.txt D:\traduc\Angband-4.2.3\lib\screens\ /Y

:: Ejecutamos juego
D:\traduc\Angband-4.2.3\angband.exe
```
## Notas originales de Angband 4.2.3

<p align="center">
  <img src="screenshots/title.png" width="425"/>
  <img src="screenshots/game.png" width="425"/>
</p>

Angband es un juego de aventura de mazmorras que utiliza carácteres de texto para representar paredes, suelos de una mazmorra y los habitantes de la misma, en la línea de juegos como NetHack y Rogue. Si necesitas ayuda en el juego, presiona `?`.

- **Instalando Angband:** Ver [Official Website](https://angband.github.io/angband/) o [compílalo por ti mismo](https://angband.readthedocs.io/en/latest/hacking/compiling.html).
- **Como jugar:** [The Angband Manual](https://angband.readthedocs.io/en/latest/)
- **Ayuda:** [Angband Forums](http://angband.oook.cz/forum/)

¡Disfruta!

-- Equipo de Desarrollo de Angband
