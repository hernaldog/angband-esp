cd src

:: Compilamos
mingw32-make -f Makefile.win

:: copiamos ejecutable
xcopy angband.exe D:\naldo\traduc\Angband-4.2.3\angband.exe /Y

:: Copia de archivos txt como portada, etc
cd ..
cd lib
cd screens
xcopy *.txt D:\naldo\traduc\Angband-4.2.3\lib\screens\ /Y

:: Ejecutamos juego
D:\naldo\traduc\Angband-4.2.3\angband.exe